<?php
  include 'config.php';
  include 'setup.php';

  $shortenedUrl = substr($_SERVER['REQUEST_URI'], 1);

  $conn = new mysqli($host, $username, $password, $database);
  if ($conn->connect_error) {
      die("Connection failed: " . $conn->connect_error);
  }

  if ($stmt = $conn->prepare("SELECT url FROM urls WHERE shortened_url = ?")) {
    $stmt->bind_param("s", $shortenedUrl);
    $stmt->execute();

    $stmt->bind_result($url);
    $stmt->fetch();

    $stmt->close();
  } else {
    echo $conn->error;
  }

  $conn->close();

  if (empty($url)) {
    echo 'not found';
  } else {
    if (empty(parse_url($url, PHP_URL_SCHEME))) {
      header('Location: http://' . $url);
    } else {
      header('Location: ' . $url);
    }
  }
