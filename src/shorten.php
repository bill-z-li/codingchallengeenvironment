<?php
  include 'config.php';
  include 'setup.php';

  $url = $_POST['url'];

  $conn = new mysqli($host, $username, $password, $database);
  if ($conn->connect_error) {
      die("Connection failed: " . $conn->connect_error);
  }

  if ($stmt = $conn->prepare("SELECT shortened_url FROM urls WHERE url = ?")) {
    $stmt->bind_param("s", $url);
    $stmt->execute();

    $stmt->bind_result($shortenedUrl);
    $stmt->fetch();

    $stmt->close();

    if (empty($shortenedUrl)) {
      $shortenedUrl = base64_encode(sha1($url, true));
      if ($stmt = $conn->prepare("INSERT INTO urls (url, shortened_url) VALUES (?, ?)")) {
          $stmt->bind_param("ss", $url, $shortenedUrl);
          $stmt->execute();
          $stmt->close();
      } else {
        echo $conn->error;
      }
    }
  } else {
    echo $conn->error;
  }

  $conn->close();
?>

<html>
  <head>
    <title>URL Shortener</title>
  </head>
  <body>
    <p><b>Your original URL: </b><?php echo htmlspecialchars($url); ?></p>
    <p><b>Is shortened to: </b><?php echo ($_SERVER['SERVER_NAME'] . ':' . $_SERVER['SERVER_PORT'] . '/' . $shortenedUrl); ?></p>
  </body>
</html>
